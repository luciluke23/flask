from flask import Flask, render_template, request, redirect, jsonify
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from pprint import pprint
from producer import my_func

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///data.db"
db = SQLAlchemy(app)

class Todo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String, nullable=False)
    value = db.Column(db.Integer, nullable=False)
    date_created = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return '<Task %r>' % self.id


@app.route('/')
@app.route('/event', methods=['POST', 'GET'])

def entry():
	if (request.method == 'POST'):
		ty = request.form['type']
		val = request.form['value']
		new_task = Todo(type=ty, value=val) # 'Todo' object has no attribute 'ty_type'
		#print("=======================s")
		try:
			db.session.add(new_task)
			db.session.commit()
			tasks = Todo.query.order_by(Todo.date_created).all()
			return redirect('/value')
		except:
			return "There was an issue."
	else:
		tasks = Todo.query.order_by(Todo.date_created).all()
		return render_template('index.html',tasks=tasks)
	
	# def data_entry():
	# 	tasks = Todo.query.order_by(Todo.date_created).all()
	# 	new_task = Todo(type=type_r, value=value_r)
	# 	try:
	# 		db.session.add(new_task)
	# 		db.session.commit()
	# 		tasks = Todo.query.order_by(Todo.date_created).all()
	# 		return render_template('index.html',tasks=tasks)
	# 	except:
	# 		return "There was an issue."


	#request.method == 'POST':
		
	# else:
	# 	tasks = Todo.query.order_by(Todo.date_created).all()
	# 	return render_template('index.html',tasks=tasks)

@app.route('/events', methods=['GET'])
def events():
	eve = Todo.query.order_by(Todo.date_created).all()
	return render_template('events.html', eve=eve)

@app.route('/value', methods=['GET'])
def value():
	eve = Todo.query.order_by(Todo.date_created).all()
	#print(eve)

	num = 0
	for i in eve:	
		#print("-",i.type,i.value,type(i.value))	
		if i.type == 'INCREMENT':
			num = (num + int(i.value))
		else:
			num = (num - int(i.value))
		
	return str(num)

@app.route('/value/<int:t>')
def t_val(t):
	eve = Todo.query.order_by(Todo.date_created).all()
	a = t
	num = 0
	lis = []
	
	if a < len(eve):
		for i in range(0,a):
			lis.append(eve[i])
		for j in lis:
			if j.type == 'INCREMENT':
				num = num + int(j.value)
			else:
				num = num - int(j.value)
		return str(num)
	else:
		return redirect('/value')

@app.route('/produce', methods=['GET'])
def producer():

    if request.method == 'GET':
        events = my_func()
        return jsonify(events)


if __name__=='__main__':
    app.debug=True
    app.run()

